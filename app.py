from flask import Flask,send_from_directory
from flask_cors import CORS
from routes.user_routes import bp as users_bp
from routes.qubic_experiment_data_routes import bp as qubic_experiment_data_bp 
from routes.data_visulization_routes import bp as data_visulization_bp
from routes.qubic_calibration_git_routes import bp as qubic_calibration_git_bp
from model.qubic_calibration_branch_model import *
from config.db import init_app
import os

def create_app():
    app = Flask(__name__)
    CORS(app, resources={r"/*": {"origins": "*"}})
    app.config.from_pyfile('config/config.py')
    init_app(app)
    app.register_blueprint(users_bp)
    app.register_blueprint(qubic_experiment_data_bp)
    app.register_blueprint(data_visulization_bp)
    app.register_blueprint(qubic_calibration_git_bp)
    setup_database(engine)

   # This route will serve your index.html file on the root path
    @app.route('/', defaults={'path': ''})
    @app.route('/<path:path>')  # This line is the catch-all route
    def catch_all(path):
        # If the requested resource exists, serve it, otherwise serve index.html
        if path and os.path.exists('qubic-data-storage-frontend/dist/' + path):
            return send_from_directory('qubic-data-storage-frontend/dist', path)
        else:
            return send_from_directory('qubic-data-storage-frontend/dist', 'index.html')

    @app.route('/assets/<path:filename>')
    def serve_static(filename):
        return send_from_directory('qubic-data-storage-frontend/dist/assets', filename)

    return app

    
    return app
if __name__ == '__main__':
    app = create_app(host='0.0.0.0', port=5000, debug=True)
    app.run()
