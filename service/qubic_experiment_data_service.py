# service.py

from model.qubic_experiment_model import QubicExperimentData

class QubicExperimentService:
    @staticmethod
    def insert_data(data):
        for qubit, data in data.items():
            qubicData = QubicExperimentData(
                qubit=qubit,
                experimentData =data
            )
            qubicData.save()

    @staticmethod
    def get_all():
        result = QubicExperimentData.get_all()
        return result

    @staticmethod
    def get_qubic_by_id(id):
        result = QubicExperimentData.get_qubic_by_id(id)
        return result
    
    @staticmethod
    def get_qubic_by_id_chip(id,chip_name):
        result = QubicExperimentData.get_qubic_by_id_chip(id,chip_name)
        return result
    
    @staticmethod
    def qubic_all_delete():
        result = QubicExperimentData.qubic_all_delete()
        return result
    
    @staticmethod
    def get_chip_names():
        result = QubicExperimentData.get_chip_names()
        return result

