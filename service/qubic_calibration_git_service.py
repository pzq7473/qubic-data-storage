# service.py
import datetime
import json

from model.qubic_calibration_branch_model import *

class CalibrationGitService:

    @staticmethod
    def get_branches():
        result = get_branches()
        return result
    
    @staticmethod
    def get_branch(branch_name):
        result = get_branch(branch_name)
        return result

    @staticmethod
    def create_branch():
        result = create_branch()
        return result
    
    @staticmethod
    def update_branch():
        result = update_branch()
        return result
    
    @staticmethod
    def delete_branch():
        result = delete_branch()
        return result
    
    @staticmethod
    def merge_branch():
            result=dolt_merge()
            return result
    
    @staticmethod
    def get_activity():
        result = get_activity()
        return result
    
    @staticmethod
    def del_all_branches(): 
        result = del_all_branches()
        return result
    
    @staticmethod
    def insert_gate_data(key,data, branch,chip_id,pulse_count):
        setup_database(engine)
        gate = {
            "gate_name" :key,
            "dest": data.get('dest', None),
            "phase" : data.get('phase', None),
            "t0":data.get('t0', None),
            "freq" :data.get('freq', None),
            "env":data.get('env', None),
            "amp":data.get('amp', None),
            "twidth":data.get('twidth', None),
            "chip_id":chip_id,
            "pulse_count": pulse_count
    }
        result = insert_data_gate(branch, gate)
        return result
    
    @staticmethod
    def insert_gate_data_reference(key,gate_name,branch,chip_id,pulse_count):
        setup_database(engine)
        data = getGateDetails(gate_name,branch)
        gate = {
            "gate_name" :key,
            "dest": data.get('dest', None),
            "phase" : data.get('phase', None),
            "t0":data.get('t0', None),
            "freq" :data.get('freq', None),
            "env":data.get('env', None),
            "amp":data.get('amp', None),
            "twidth":data.get('twidth', None),
            "chip_id":chip_id,
            "pulse_count": pulse_count
        }
        result = insert_data_gate(branch,gate)
        return result

    @staticmethod
    def insert_qubit_data(key, data, branch,chip_id):
        setup_database(engine)
        qubit  = {
        'name': key,
        'freq': data.get('freq', None),  
        'freq_predicted': data.get('freq_predicted', None),
        'readfreq': data.get('readfreq', None),
        'freq_ef': data.get('freq_ef', None),
        'chip_id':chip_id
        }
        result = insert_data_qubit(branch,qubit)
        return result


    # Perform the insert operation for gate table
    @staticmethod
    def get_allData(branch,chip_id):
            result = get_all_data(branch,chip_id)
            return result
    
    @staticmethod
    def get_all_data_by_property(branch,chip_id):
            result = get_all_data_by_property(branch,chip_id)
            return result
    
    @staticmethod
    def print_commit_log(branch_name):
            result = print_commit_log(branch_name)
            return result
    
    @staticmethod
    def get_commit_specific_data(hash,branch_name):
         result = get_commit_specific_data(hash,branch_name)
         return result
    
    @staticmethod
    def get_commit_diff(hash,branch_name):
         result = get_commit_diff(hash,branch_name)
         return result

    @staticmethod 
    def insert_chip_data(branch, chipData):
        setup_database(engine)
        result = insert_data_chip(branch, chipData)
        return result

    @staticmethod
    def get_chip_data(branch_name):
        result = get_chip_data(branch_name)
        return result
    
    @staticmethod
    def check_existing_data(branch):
         result = check_data(branch)
         return result