# service.py

from model.user_model import User

class UserService:
    @staticmethod
    def create_user(username, password):
        user = User(username, password)
        user.save()

    @staticmethod
    def get_all_users():
        result = User.get_all()
        return result

    @staticmethod
    def get_user_by_username(username):
        result = User.get_by_username(username)
        return result
