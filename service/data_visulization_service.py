# service.py
import datetime
import json

from model.qubic_experiment_model import QubicExperimentData

class DataVisulizationService:

    @staticmethod
    def get_count_all_qubits():
        result = QubicExperimentData.get_count_all_qubits()
        return result

    @staticmethod
    def get_count_all_experiments():
        result = QubicExperimentData.get_count_all_experiments()
        return result
    
    @staticmethod
    def get_all_prep0read1(chip_name):
        result = QubicExperimentData.get_all_prep0read1(chip_name)
        return result
    
    @staticmethod
    def get_all_prep1read0(chip_name):
        result = QubicExperimentData.get_all_prep1read0(chip_name)
        return result
    
    @staticmethod
    def get_all_separation(chip_name):
        result = QubicExperimentData.get_all_separation(chip_name)
        return result
    
    @staticmethod
    def get_all_t1(chip_name):
        result = QubicExperimentData.get_all_t1(chip_name)
        return result
    
    @staticmethod
    def get_all_t2ramsey(chip_name):
        result = QubicExperimentData.get_all_t2ramsey(chip_name)
        return result
    
    @staticmethod
    def get_all_t2spinecho(chip_name):
        result = QubicExperimentData.get_all_t2spinecho(chip_name)
        return result
    
    @staticmethod
    def get_all_fidelityData(chip_name):
        result = QubicExperimentData.get_all_fidelityData(chip_name)
        return result

    @staticmethod
    def get_each_qubits_properties(qubit_number,chip_name):
        result = QubicExperimentData.get_each_qubits_properties(qubit_number,chip_name)
        return result