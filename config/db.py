# db.py

from flask_pymongo import PyMongo
from flask import current_app

mongo = PyMongo()

def init_app(app):
    with app.app_context():
        mongo.init_app(app, uri=current_app.config['MONGO_URI'])

