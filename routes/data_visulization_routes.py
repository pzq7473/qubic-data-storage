# routes.py

from flask import Blueprint, jsonify, render_template, request
from controller.data_visulization_controller import DataVisulizationController

bp = Blueprint('data_visulization', __name__, url_prefix='/dataVisulization')

@bp.route('/fidelity/<string:chip_name>', methods=['GET'])
def get_all_fidelityData(chip_name):
        result = DataVisulizationController.get_all_fidelityData(chip_name)
        return jsonify(result)

@bp.route('/prep0read1/<string:chip_name>', methods=['GET'])
def get_all_prep0read1(chip_name):
        result = DataVisulizationController.get_all_prep0read1(chip_name)
        return jsonify(result)

@bp.route('/prep1read0/<string:chip_name>', methods=['GET'])
def get_all_prep1read0(chip_name):
        result = DataVisulizationController.get_all_prep1read0(chip_name)
        return jsonify(result)

@bp.route('/separation/<string:chip_name>', methods=['GET'])
def get_all_separation(chip_name):
        result = DataVisulizationController.get_all_separation(chip_name)
        return jsonify(result)

@bp.route('/t1/<string:chip_name>', methods=['GET'])
def get_all_t1(chip_name):
        result = DataVisulizationController.get_all_t1(chip_name)
        return jsonify(result)

@bp.route('/t2spinecho/<string:chip_name>', methods=['GET'])
def get_all_t2spinecho(chip_name):
        result = DataVisulizationController.get_all_t2spinecho(chip_name)
        return jsonify(result)

@bp.route('/t2ramsey/<string:chip_name>', methods=['GET'])
def get_all_t2ramsey(chip_name):
        result = DataVisulizationController.get_all_t2ramsey(chip_name)
        return jsonify(result)

@bp.route('/experiments_count', methods=['GET'])
def get_count_all_experiments():
        result = DataVisulizationController.get_count_all_experiments()
        return jsonify(result)

@bp.route('/qubits_count', methods=['GET'])
def get_count_all_qubits():
        result = DataVisulizationController.get_count_all_qubits()
        return jsonify(result)

@bp.route('/individual/qubit/properties/<string:qubit_number>/<string:chip_name>', methods=['GET'])
def get_each_qubits_properties(qubit_number,chip_name):
        result = DataVisulizationController.get_each_qubits_properties(qubit_number,chip_name)
        return jsonify(result)