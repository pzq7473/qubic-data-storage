# routes.py

from flask import Blueprint, jsonify, render_template, request
from controller.qubic_experiment_data_controller import QubicExperimentController

bp = Blueprint('qubic_experiment_data', __name__, url_prefix='/qubic/experiment')

@bp.route('', methods=['GET', 'POST'])
def qubic():
    if request.method == 'GET':
        result = QubicExperimentController.get_all()
        return jsonify(result)
    elif request.method == 'POST':
        return QubicExperimentController.insert_data()

@bp.route('/<string:id>', methods=['GET'])
def qubic_by_id(id):
    result = QubicExperimentController.get_qubic_by_id(id)
    return jsonify(result)

@bp.route('/deleteAll', methods=['DELETE'])
def qubic_all_delete():
    result = QubicExperimentController.qubic_all_delete()
    return jsonify(result)

@bp.route('/get/chips', methods=['GET'])
def get_chip_names():
    result = QubicExperimentController.get_chip_names()
    return jsonify(result)

@bp.route('/<string:id>/<string:chip_name>', methods=['GET'])
def qubic_by_id_chip(id,chip_name):
    result = QubicExperimentController.get_qubic_by_id(id,chip_name)
    return jsonify(result)