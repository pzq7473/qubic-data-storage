# routes.py

from flask import Blueprint, jsonify, render_template, request
from controller.user_controller import UserController

bp = Blueprint('users', __name__, url_prefix='/users')

@bp.route('', methods=['GET', 'POST'])
def users():
    if request.method == 'GET':
        result = UserController.get_all_users()
        return jsonify(result)
    elif request.method == 'POST':
        return UserController.create_user()

@bp.route('/<string:username>', methods=['GET'])
def user(username):
    result = UserController.get_user_by_username(username)
    return jsonify(result)
