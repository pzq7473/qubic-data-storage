# routes.py

from flask import Blueprint, jsonify, render_template, request
from controller.qubic_calibration_git_controller import CalibrationGitController

bp = Blueprint('calibration_git', __name__, url_prefix='/git')

@bp.route('/branches', methods=['GET'])
def get_branches():
        result = CalibrationGitController.get_branches()
        return result

@bp.route('/all_branches', methods=['DELETE'])
def del_all_branches():
        result = CalibrationGitController.del_all_branches()
        return result

@bp.route('/branches/<string:branch_name>', methods=['GET'])
def get_branch(branch_name):
        result = CalibrationGitController.get_branch(branch_name)
        return result

@bp.route('/branches', methods=['POST'])
def create_branch():
        result = CalibrationGitController.create_branch()
        return result

@bp.route('/branches', methods=['PUT'])
def update_branch():
        result = CalibrationGitController.update_branch()
        return result

@bp.route('/branches', methods=['DELETE'])
def delete_branch():
        result = CalibrationGitController.delete_branch()
        return result

@bp.route('/branches/merge', methods=['POST'])
def merge_branch():
        result = CalibrationGitController.merge_branch()
        return result

@bp.route('/activity', methods=['GET'])
def get_activity():
        result = CalibrationGitController.get_activity()
        return result

@bp.route('/insert_data', methods=['POST'])
def insert_data():
        result = CalibrationGitController.insert_data()
        return result

@bp.route('/chip/insert_data', methods=['POST'])
def insert_chip_data():
        result = CalibrationGitController.insert_chip_data()
        return result

@bp.route('/get/chip/<string:branch_name>', methods=['GET'])
def get_chip_data(branch_name):
        result = CalibrationGitController.get_chip_data(branch_name)
        return result

@bp.route('/get_all/<string:branch_name>/<string:chip_id>', methods=['GET'])
def get_allData(branch_name,chip_id):
        print(branch_name)
        result = CalibrationGitController.get_allData(branch_name,chip_id)
        return result

@bp.route('/get_all_by_property/<string:branch_name>/<string:chip_id>', methods=['GET'])
def get_all_data_by_property(branch_name,chip_id):
        result = CalibrationGitController.get_all_data_by_property(branch_name,chip_id)
        return result

@bp.route('/branch/commit_log/<string:branch_name>', methods=['GET'])
def print_commit_log(branch_name):
        result = CalibrationGitController.print_commit_log(branch_name)
        return result

@bp.route('/get/commit/<string:hash_commit>/<string:branch_name>', methods=['GET'])
def get_commit_specific_data(hash_commit,branch_name):
        result = CalibrationGitController.get_commit_specific_data(hash_commit,branch_name)
        return result

@bp.route('/get/diff/<string:hash_commit>/<string:branch_name>', methods=['GET'])
def get_commit_diff(hash_commit,branch_name):
        result = CalibrationGitController.get_commit_diff(hash_commit,branch_name)
        return result