# controller.py

from flask import request
from service.qubic_experiment_data_service import QubicExperimentService

class QubicExperimentController:
    @staticmethod
    def insert_data():
        data = request.get_json()
        QubicExperimentService.insert_data(data)
        return {"message": "Qubic Data Inserted"}, 201

    @staticmethod
    def get_all():
        result = QubicExperimentService.get_all()
        return result

    @staticmethod
    def get_qubic_by_id(id):
        result = QubicExperimentService.get_qubic_by_id(id)
        return result
    
    @staticmethod
    def get_qubic_by_id_chip(id,chip_name):
        result = QubicExperimentService.get_qubic_by_id_chip(id,chip_name)
        return result
    
    @staticmethod
    def qubic_all_delete():
        result = QubicExperimentService.qubic_all_delete()
        return result

    @staticmethod
    def get_chip_names():
        result = QubicExperimentService.get_chip_names()
        return result
