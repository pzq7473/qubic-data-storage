# controller.py

from flask import request
from service.user_service import UserService

class UserController:
    @staticmethod
    def create_user():
        data = request.get_json()
        UserService.create_user(data['username'], data['password'])
        return {"message": "User created"}, 201

    @staticmethod
    def get_all_users():
        result = UserService.get_all_users()
        return result

    @staticmethod
    def get_user_by_username(username):
        result = UserService.get_user_by_username(username)
        return result
