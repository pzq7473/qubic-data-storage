# controller.py

from flask import request
from service.qubic_calibration_git_service import CalibrationGitService
from model.qubic_calibration_branch_model import *

class CalibrationGitController:
    @staticmethod
    def create_branch():
        result = CalibrationGitService.create_branch()
        return result

    @staticmethod
    def get_branches():
        result = CalibrationGitService.get_branches()
        return result
    
    @staticmethod
    def del_all_branches():
        result = CalibrationGitService.del_all_branches()
        return result
    

    @staticmethod
    def get_branch(branch_name):
        result = CalibrationGitService.get_branch(branch_name)
        return result

    @staticmethod
    def update_branch():
        result = CalibrationGitService.update_branch()
        return result
    
    @staticmethod
    def delete_branch():
        result = CalibrationGitService.delete_branch()
        return result
    
    @staticmethod
    def merge_branch():
         result = CalibrationGitService.merge_branch()
         return result

    @staticmethod
    def get_activity():
        result = CalibrationGitService.get_activity()
        return result
    
    @staticmethod
    def insert_data():
        data = request.get_json()
        apiData =data['data']
        branch =data['branch_name']
        commit = data['commit']
        chip_id = data['chip_id']

        for qubit_key,qubit in apiData['Qubits'].items():
            CalibrationGitService.insert_qubit_data(qubit_key,qubit, branch,chip_id)
        for gate_name, gate_info_list in apiData['Gates'].items():
            pulse_count = 0  # Initialize pulse count for each gate
            for gate_info in gate_info_list:
                if isinstance(gate_info, list):
                    for single_gate_info in gate_info:
                        if isinstance(single_gate_info, dict):
                            pulse_count += 1  # Increment pulse count for each pulse
                            CalibrationGitService.insert_gate_data(gate_name, single_gate_info, branch,chip_id,pulse_count)
                        else:
                            CalibrationGitService.insert_gate_data_reference(gate_name, single_gate_info, branch,chip_id,pulse_count)
                elif isinstance(gate_info, dict):
                    pulse_count += 1  # Increment pulse count for each pulse
                    CalibrationGitService.insert_gate_data(gate_name, gate_info, branch,chip_id,pulse_count)
                else:
                    CalibrationGitService.insert_gate_data_reference(gate_name, gate_info, branch,chip_id,pulse_count)

        engine = dolt_checkout(branch)
        author_name = commit['author_name']
        author_email =commit['author_email']
        author = f"{author_name} <{author_email}>"
        dolt_commit(engine,author, commit['message'])
        return {"message": "Qubic Data Inserted"}, 201
    
    @staticmethod
    def insert_chip_data():
            data = request.get_json()   
            apiData =data['data']
            branch =data['branch_name']
            result = CalibrationGitService.insert_chip_data(branch,apiData)
            return result

    @staticmethod
    def get_chip_data(branch_name):
            result = CalibrationGitService.get_chip_data(branch_name)
            return result

    @staticmethod
    def get_allData(branch_name,chip_id):
            result = CalibrationGitService.get_allData(branch_name,chip_id)
            return result
    
    @staticmethod
    def get_all_data_by_property(branch_name,chip_id):
            result = CalibrationGitService.get_all_data_by_property(branch_name,chip_id)
            return result
    
    @staticmethod
    def print_commit_log(branch_name):
            result = CalibrationGitService.print_commit_log(branch_name)
            return result
    
    @staticmethod
    def get_commit_specific_data(hash,branch_name):
         result =CalibrationGitService.get_commit_specific_data(hash,branch_name)
         return result

    @staticmethod
    def get_commit_diff(hash,branch_name):
         result =CalibrationGitService.get_commit_diff(hash,branch_name)
         return result