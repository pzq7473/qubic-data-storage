# model.py

from flask import jsonify
from config.db import mongo
from bson import ObjectId

class QubicExperimentData:
    def __init__(self, qubit, experimentData):
        self.qubit = qubit
        self.experimentData = experimentData

    def save(self):
        qubits = mongo.db.qubits
        qubit_filter = {"qubit": self.qubit}
        qubit_update = {"$push": {"experimentData": self.experimentData}}
        qubits.update_one(qubit_filter, qubit_update, upsert=True)

    @staticmethod
    def get_all():
        qubits = mongo.db.qubits
        result = qubits.find()
        json_result = []
        for qubit in result:
            qubit['_id'] = str(qubit['_id'])
            json_result.append(qubit)
        return json_result

    @staticmethod
    def get_qubic_by_id(id):
        qubits = mongo.db.qubits
        result = qubits.find({"qubit": id})
        output = []
        for document in result:
            document['_id'] = str(document['_id'])  # Convert ObjectId to string
            output.append(document)
        return output
    
    @staticmethod
    def get_qubic_by_id_chip(id,chip_name):
        qubits = mongo.db.qubits
        result = qubits.find({"qubit": id})
        output = []
        for document in result:
            experiment_data_with_chip_name = [
                exp_data for exp_data in document.get('experimentData', []) 
                if exp_data.get('chip_name') == chip_name
            ]
            if experiment_data_with_chip_name:
                new_doc = {
                    "qubit": document['qubit'],
                    "experimentData": experiment_data_with_chip_name
                }
                output.append(new_doc)
        return output
    
    @staticmethod
    def qubic_all_delete():
        qubits = mongo.db.qubits
        result = qubits.delete_many({})
        json_result = ["All Deleted"]
        return json_result
    
    @staticmethod
    def get_all_fidelityData(chip_name):
        qubits = mongo.db.qubits
        cursor = qubits.find({}, {"qubit": 1,"experimentData.chip_name":1, "experimentData.rb1qinfidelity": 1})
        result = {}
        for doc in cursor:
            qubit = doc["qubit"]
            experimentData = doc.get("experimentData", [])
            fidelityData = []
            for data in experimentData:
                if data.get("chip_name") == chip_name:
                    rb1qinfidelity = data.get("rb1qinfidelity", {})
                    fidelityData.append(rb1qinfidelity)
            result[qubit] = fidelityData
        return result

    
    @staticmethod
    def get_all_prep0read1(chip_name):
        qubits = mongo.db.qubits
        cursor = qubits.find({}, {"qubit": 1, "experimentData.chip_name":1, "experimentData.prep0read1": 1 })
        result = {}
        for doc in cursor:
            qubit = doc["qubit"]
            experimentData = doc.get("experimentData", [])
            prep0read1Data = []
            for data in experimentData:
                if data.get("chip_name") == chip_name:
                    prep0read1 = data.get("prep0read1", {})
                    prep0read1Data.append(prep0read1)
            result[qubit] = prep0read1Data
        return result
    
    @staticmethod
    def get_all_prep1read0(chip_name):
        qubits = mongo.db.qubits
        cursor = qubits.find({}, {"qubit": 1,"experimentData.chip_name":1, "experimentData.prep1read0": 1})
        result = {}
        for doc in cursor:
            qubit = doc["qubit"]
            experimentData = doc.get("experimentData", [])
            prep1read0Data = []
            for data in experimentData:
                if data.get("chip_name") == chip_name:
                    prep1read0 = data.get("prep1read0", {})
                    prep1read0Data.append(prep1read0)
            result[qubit] = prep1read0Data
        return result
    
    @staticmethod
    def get_all_separation(chip_name):
        qubits = mongo.db.qubits
        cursor = qubits.find({}, {"qubit": 1,"experimentData.chip_name":1, "experimentData.separation": 1})
        result = {}
        for doc in cursor:
            qubit = doc["qubit"]
            experimentData = doc.get("experimentData", [])
            separationData = []
            for data in experimentData:
                if data.get("chip_name") == chip_name:
                    separation = data.get("separation", {})
                    separationData.append(separation)
            result[qubit] = separationData
        return result
    
    @staticmethod
    def get_all_t1(chip_name):
        qubits = mongo.db.qubits
        cursor = qubits.find({}, {"qubit": 1,"experimentData.chip_name":1, "experimentData.t1": 1})
        result = {}
        for doc in cursor:
            qubit = doc["qubit"]
            experimentData = doc.get("experimentData", [])
            t1Data = []
            for data in experimentData:
                if data.get("chip_name") == chip_name:
                    t1 = data.get("t1", {})
                    t1Data.append(t1)
            result[qubit] = t1Data
        return result
    
    @staticmethod
    def get_all_t2ramsey(chip_name):
        qubits = mongo.db.qubits
        cursor = qubits.find({}, {"qubit": 1,"experimentData.chip_name":1, "experimentData.t2ramsey": 1})
        result = {}
        for doc in cursor:
            qubit = doc["qubit"]
            experimentData = doc.get("experimentData", [])
            t2ramseyData = []
            for data in experimentData:
                if data.get("chip_name") == chip_name:
                    t2ramsey = data.get("t2ramsey", {})
                    t2ramseyData.append(t2ramsey)
            result[qubit] = t2ramseyData
        return result
    
    @staticmethod
    def get_all_t2spinecho(chip_name):
        qubits = mongo.db.qubits
        cursor = qubits.find({}, {"qubit": 1,"experimentData.chip_name":1, "experimentData.t2spinecho": 1})
        result = {}
        for doc in cursor:
            qubit = doc["qubit"]
            experimentData = doc.get("experimentData", [])
            t2spinechoData = []
            for data in experimentData:
                if data.get("chip_name") == chip_name:
                    t2spinecho = data.get("t2spinecho", {})
                    t2spinechoData.append(t2spinecho)
            result[qubit] = t2spinechoData
        return result
    
    @staticmethod
    def get_count_all_experiments():
        qubitCount = mongo.db.qubits
        totalExperimentCount = qubitCount.find({}, {"qubit": "Q7", "experimentData": 1})
        array_size = 0
        for doc in totalExperimentCount:
            array_size = len(doc.get('experimentData', []))
        return array_size   

    @staticmethod
    def get_count_all_qubits():
        qubitCount = mongo.db.qubits
        totalQubits = qubitCount.count_documents({})
        return totalQubits
    
    @staticmethod
    def get_each_qubits_properties(qubit_number, chip_name):
        qubits = mongo.db.qubits
        # Aggregation pipeline
        pipeline = [
            {
                '$match': {
                    'qubit': qubit_number, 
                    'experimentData.chip_name': chip_name
                }
            }, {
                '$project': {
                    'qubit': 1,
                    'experimentData': {
                        '$filter': {
                            'input': '$experimentData', 
                            'as': 'data', 
                            'cond': {
                                '$eq': ['$$data.chip_name', chip_name]
                            }
                        }
                    }
                }
            }
        ]
        cursor = qubits.aggregate(pipeline)
        result = {}
        for doc in cursor:
            qubit = doc["qubit"]  # Convert qubit to string
            experimentData = doc.get("experimentData", [])
            prep0read1 = [data.get("prep0read1", {}) for data in experimentData]
            prep1read0 = [data.get("prep1read0", {}) for data in experimentData]
            rb1qinfidelity = [data.get("rb1qinfidelity", {}) for data in experimentData]
            separation = [data.get("separation", {}) for data in experimentData]
            t1 = [data.get("t1", {}) for data in experimentData]
            t2ramsey = [data.get("t2ramsey", {}) for data in experimentData]
            t2spinecho = [data.get("t2spinecho", {}) for data in experimentData]
            result[qubit] = {
                "prep0read1": prep0read1,
                "prep1read0": prep1read0,
                "rb1qinfidelity": rb1qinfidelity,
                "separation": separation,
                "t1": t1,
                "t2ramsey": t2ramsey,
                "t2spinecho": t2spinecho
            }
        return result

    @staticmethod
    def get_chip_names(qubit_number='Q0'):
        qubits = mongo.db.qubits
        pipeline = [
        {
            '$match': {
                'qubit': 'Q0'
            }
        }, {
            '$project': {
                'chip_names': {
                    '$filter': {
                        'input': {
                            '$map': {
                                'input': '$experimentData',
                                'as': 'expData',
                                'in': '$$expData.chip_name'
                            }
                        },
                        'as': 'chipName',
                        'cond': {
                            '$ne': ['$$chipName', None]
                        }
                    }
                },
                '_id': 0
            }
        }
    ]  
        try:
            qubits = mongo.db.qubits
            result_cursor = qubits.aggregate(pipeline)
            chip_names = set()
            for doc in result_cursor:
                chip_names.update(doc.get('chip_names', []))
            chip_names_list = list(chip_names)
            return chip_names_list
        except Exception as e:
            return jsonify({"error": str(e)}), 500