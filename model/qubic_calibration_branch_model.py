from flask import Flask, jsonify, request
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, Session, sessionmaker
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.sql import func
import logging
logging.basicConfig(filename='logging/database_logger.log', level=logging.ERROR, 
                    format='%(asctime)s:%(levelname)s:%(message)s')

# from sqlalchemy import create_engine, text, MetaData, Table, Column, Integer, String, select
from sqlalchemy import (
    create_engine,
    text,
    Table,
    Column,
    Integer,
    String,
    Date,
    select,
    insert,
    update,
    delete,
    ForeignKey,
    MetaData,
    Float,
    JSON,
    DateTime,
)

engine = create_engine("mysql+mysqlconnector://root@doltdb/calibration")
con = engine.connect()
Session = sessionmaker(bind=engine)
metadata_obj = MetaData()
dolt_branches = Table(
    "dolt_branches",
    metadata_obj,
    autoload_with=engine,
    # autoload=True
)
dolt_log = Table(
    "dolt_log",
    metadata_obj,
    autoload_with=engine,
    # autoload=True
)

def setup_database(engine):
    metadata_obj = MetaData()

    chip = Table(
            'chip',
            metadata_obj,
            Column('id', Integer, primary_key=True, autoincrement=True),
            Column('name', String(255), nullable=False),  # Assuming chip name is mandatory
            Column('timestamp', DateTime, server_default=func.now())        
        )

    # This is standard SQLAlchemy without the ORM
    gate = Table(
        'gate',
        metadata_obj,
        Column('id', Integer, primary_key=True, autoincrement=True),
        Column('gate_name', String(255), nullable=True),
        Column('dest', String(255), nullable=True),
        Column('phase', String(255), nullable=True),
        Column('t0', Float, nullable=True),
        Column('freq', String(255), nullable=True),
        Column('env', JSON, nullable=True),
        Column('amp', Float, nullable=True),
        Column('twidth', Float, nullable=True),
        Column('chip_id', Integer, ForeignKey('chip.id'), nullable=False),  # Establishing the foreign key relationship
        Column('pulse_count', Integer, nullable=True),  # New column for pulse count
        Column('timestamp', DateTime, server_default=func.now())
    )

    # Modifying the 'qubit' table to include the 'chip_id' foreign key
    qubit = Table(
        'qubit',
        metadata_obj,
        Column('id', Integer, primary_key=True, autoincrement=True),
        Column('name', String(255), nullable=True),
        Column('freq', Float, nullable=True),
        Column('freq_predicted', Float, nullable=True),
        Column('readfreq', Float, nullable=True),
        Column('freq_ef', Float, nullable=True),
        Column('chip_id', Integer, ForeignKey('chip.id'), nullable=False),  # Establishing the foreign key relationship
        Column('timestamp', DateTime, server_default=func.now())
    )
    metadata_obj.create_all(engine)

def load_tables(engine):
        metadata_obj = MetaData()
        gate = Table("gate", metadata_obj, autoload_with=engine)
        qubit = Table("qubit", metadata_obj, autoload_with=engine)
        chip = Table("chip", metadata_obj, autoload_with=engine)
        return (qubit,gate,chip)

def get_branches():
    try: 
        with Session() as session:
            select_stmt = select(dolt_branches)
            results_proxy = session.execute(select_stmt)
            rows = []
            column_names = results_proxy.keys()
            
            for row in results_proxy:
                row_dict = dict(zip(column_names, row))
                rows.append(row_dict)
            
            return jsonify(rows)
        
    except SQLAlchemyError as e:
                logging.error("An error occurred: %s", str(e))
                raise e 


def get_branch(branch_name):
    try: 
        with Session() as session:
            select_stmt = select(dolt_branches).where(dolt_branches.c.name == branch_name).order_by(dolt_branches.c.latest_commit_date.desc())
            result_proxy = session.execute(select_stmt)
            column_names = result_proxy.keys()
            row = result_proxy.fetchone()
            if row:
                row_dict = dict(zip(column_names, row))
                return jsonify(row_dict)
            else:
                return jsonify({'message': 'Branch not found'}) 
    except SQLAlchemyError as e:
            logging.error("An error occurred: %s", str(e))
            raise e    

def get_activity():
    try: 
        stmt1 = text("use calibration;")
        stmt2 = text("SELECT * FROM dolt_log;")
        with Session() as session:
            session.execute(stmt1)
            results_proxy = session.execute(stmt2)
            rows = []
            column_names = results_proxy.keys()
            for row in results_proxy:
                row_dict = dict(zip(column_names, row))
                rows.append(row_dict)
            return jsonify(rows)
    except SQLAlchemyError as e:
            logging.error("An error occurred: %s", str(e))
            raise e  

def create_branch():
    try:
        session = Session()
        branch_name = request.json['branch_name']
        call_stmt =  text("call dolt_branch(:branch_name)")
        session.execute(call_stmt, {"branch_name":branch_name})

        author_name = request.json['author_name']
        author_email =request.json['author_email']
        message = request.json['author_message']
        author = f"{author_name} <{author_email}>"

        call_stmt1 = text("call dolt_commit('--allow-empty', '--author', :author, '-m', :message)")
        session.execute(call_stmt1, {"author": author, "message":message} )

        engine = dolt_checkout(branch_name)
        session.commit()
        get_branches()

        response_message = {
            'status': 'success',
            'message': 'Branch created successfully',
            'branch': branch_name
        }
        return jsonify(response_message), 201
    except Exception as e:
            print(e)
            logging.error("An error occurred: %s", str(e))
            session.rollback()
            response_message = {
            'status': 'failure',
            'message': 'Already Branch Exisist',
            'branch': branch_name
        }
            return jsonify(response_message), 400
    finally:
            session.close()


def update_branch():
    try: 
        session = Session()
        new_branch_name = request.json['new_branch_name']
        old_branch_name = request.json['old_branch_name']
        call_stmt =  text("call dolt_branch('-m',:old_branch_name,:new_branch_name)")
        session.execute(call_stmt, {"new_branch_name": new_branch_name, "old_branch_name": old_branch_name})
        dolt_checkout(new_branch_name)
        author_name = request.json['author_name']
        author_email =request.json['author_email']
        message = old_branch_name + 'Changed to ' + new_branch_name
        author = f"{author_name} <{author_email}>"

        call_stmt1 = text("call dolt_commit('--allow-empty', '--author', :author, '-m', :message)")
        session.execute(call_stmt1, {"author": author, "message":message} )
        session.commit()
        return jsonify({'message': 'Branch updated successfully'})
    except Exception as e:
        session.rollback()
        error_message = {
                'status': 'error',
                'message': 'can not update',
                'error_details': str(e)
            }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400
    finally:
        session.close()


def delete_branch():
    try:
        session = Session()
        dolt_checkout("main")
        branch_name = request.json['branch_name']
        call_stmt =  text("call dolt_branch('-d','-f',:branch_name)")
        session.execute(call_stmt, {"branch_name": branch_name})
        author_name = request.json['author_name']
        author_email =request.json['author_email']
        message = branch_name+ "Deleted Successfully"
        author = f"{author_name} <{author_email}>"
        call_stmt1 = text("call dolt_commit('--allow-empty', '--author', :author, '-m', :message)")
        session.execute(call_stmt1, {"author": author, "message":message} )
        session.commit()
        return jsonify({'message': 'Branch deleted successfully'})
    except Exception as e:
        session.rollback()
        error_message = {
                    'status': 'error',
                    'message': 'can not delete',
                    'error_details': str(e)
                }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400
    finally:
        session.close()   

def copy_branch():
    try:
        session = Session()
        new_branch_name = request.json['new_branch_name']
        original_branch_name = request.json['original_branch_name']
        call_stmt =  text("call dolt_branch('-c',:original_branch_name,:new_branch_name)")
        session.execute(call_stmt, {"new_branch_name": new_branch_name, "original_branch_name": original_branch_name})
        dolt_checkout("main")
        author_name = request.json['author_name']
        author_email =request.json['author_email']
        message = new_branch_name+ "successfully copied from " + original_branch_name
        author = f"{author_name} <{author_email}>"
        call_stmt1 = text("call dolt_commit('--allow-empty', '--author', :author, '-m', :message)")

        session.execute(call_stmt1, {"author": author, "message":message} )
        session.commit()
        return jsonify({'message': 'Branch copied successfully'})
    except Exception as e:
        session.rollback()
        error_message = {
                    'status': 'error',
                    'message': 'can not copy',
                    'error_details': str(e)
                }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400
    finally:
        session.close()  

def dolt_merge():
    try:
        from_branch=request.json['from_branch']
        to_branch=request.json['to_branch']
        engine = dolt_checkout(from_branch)
        stmt = text("CALL DOLT_MERGE('" + to_branch + "')")
        session = Session(bind=engine)
        with session.begin():
            results = session.execute(stmt)
            rows = results.fetchall()
            commit = rows[0][0]
            fast_forward = rows[0][1]
            # conflicts = rows[0][2]
            merge_result = {
                'to_branch': to_branch,
                'commit': commit,
                'fast_forward': fast_forward,
                # 'conflicts': conflicts
            }
            author_name = request.json['author_name']
            author_email =request.json['author_email']
            message = from_branch + " is merged to " + to_branch
            author = f"{author_name} <{author_email}>"
            call_stmt1 = text("call dolt_commit('--allow-empty', '--author', :author, '-m', :message)")
            session.execute(call_stmt1, {"author": author, "message":message} )
            session.commit()
            dolt_checkout(to_branch)
            return jsonify(merge_result)
    except Exception as e:
        session.rollback()
        error_message = {
                    'status': 'error',
                    'message': 'can not merge',
                    'error_details': str(e)
                }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400
    finally:
        session.close()  

def del_all_branches():
    try: 
        metadata_obj = MetaData()
        dolt_checkout("main")
        dolt_branches = Table("dolt_branches", metadata_obj, autoload_with=engine)
        stmt = select(dolt_branches.c.name).where(dolt_branches.c.name != 'main')
        session = Session(bind=engine)
        with session.begin():
            results = session.execute(stmt)
            for row in results:
                branch = row[0]
                stmt = text("CALL DOLT_BRANCH('-D', '" + branch + "')")
                session.execute(stmt)
        session.commit()
        return jsonify({'message': 'All Branches deleted successfully'})
    except Exception as e:
        session.rollback()
        error_message = {
                    'status': 'error',
                    'message': 'del_all error',
                    'error_details': str(e)
                }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400
    finally:
        session.close()


def dolt_checkout(branch):
    engine_base = "mysql+mysqlconnector://root@doltdb/calibration"
    engine = create_engine(
    	engine_base + "/" + branch
    )
    return engine

# def dolt_checkout(branch):
#     engine_base = "mysql+mysqlconnector://root@127.0.0.1:3306/calibration"
#     engine = create_engine(
#     	engine_base + "/" + branch
#     )
#     return engine
def print_active_branch():
    try:
        session = Session()
        dolt_checkout("main")
        stmt = text("select active_branch()")
        with session.begin():
            results = session.execute(stmt)
            rows = results.fetchall()
            active_branch = rows[0][0]
        session.commit()
        print("Active branch: " + active_branch)
    except Exception as e:
        session.rollback()
        error_message = {
                    'status': 'error',
                    'message': 'error print active',
                    'error_details': str(e)
                }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400
    finally:
        session.close()          

def getGateDetails(gate_name,branch):
    try: 
        engine = dolt_checkout(branch)
        Session = sessionmaker(bind=engine)
        session = Session()
        with session.begin():
            stmt = text("select * from gate where gate_name=:gate_name" )
            gate_data = session.execute(stmt,{"gate_name": gate_name}).fetchone() 
            if gate_data:
                gate_data_dict = {
                        "id": gate_data[0],
                        "gate_name": gate_data[1],
                        "dest": gate_data[2],
                        "phase": gate_data[3],
                        "t0": gate_data[4],
                        "freq": gate_data[5],
                        "env": gate_data[6],
                        "amp": gate_data[7],
                        "twidth": gate_data[8],
                        "chip_id": gate_data[9]
                }
            else:
                gate_data_dict = {}  # Empty dictionary if no data found 
        session.commit()
        return gate_data_dict
    except Exception as e:
        session.rollback()
        error_message = {
                    'status': 'error',
                    'message': 'get agte details',
                    'error_details': str(e)
                }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400
    finally:
        session.close()

def insert_data_chip(branch, chipData):
    try: 
        engine = dolt_checkout(branch)  # Assuming dolt_checkout is a function you've previously defined
        Session = sessionmaker(bind=engine)
        session = Session()
        setup_database(engine)
        (qubit, gate, chip) = load_tables(engine)    
        with session.begin():
            existing_chip = session.query(chip).filter_by(name=chipData['name']).first()
            if existing_chip is not None:
                return jsonify({'message': 'Chip name already exists'}), 400
            stmt = insert(chip).values(chipData)
            session.execute(stmt)
        session.commit()
        return jsonify({'message': 'data inserted'})
    except Exception as e:
        session.rollback()
        error_message = {
                    'status': 'error',
                    'message': 'Inser data chip',
                    'error_details': str(e)
                }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400
    finally:
        session.close()   

def insert_data_gate(branch, gateData):
    try:
        engine = dolt_checkout(branch)
        Session = sessionmaker(bind=engine)
        session = Session()
        (qubit, gate, chip) = load_tables(engine)

        # Sanitize the input data
        for key, value in gateData.items():
            if value == '' or value == 'null' or str(value).lower() == 'null':
                gateData[key] = None

        with session.begin():
            # Check if the row exists
            existing_gate = session.query(gate).filter(
                gate.c.gate_name == gateData["gate_name"],
                gate.c.pulse_count == gateData["pulse_count"],
                gate.c.chip_id == gateData["chip_id"]
            ).first()

            if existing_gate:
                # Update the existing row
                stmt = update(gate).where(
                    gate.c.gate_name == gateData["gate_name"],
                    gate.c.pulse_count == gateData["pulse_count"],
                    gate.c.chip_id == gateData["chip_id"]
                ).values(gateData)
            else:
                # Insert a new row
                stmt = insert(gate).values(gateData)

            session.execute(stmt)
        session.commit()
        return jsonify({'message': 'Data inserted or updated successfully'})
    except Exception as e:
        session.rollback()
        error_message = {
            'status': 'error',
            'message': 'Insert/update gate error',
            'error_details': str(e)
        }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400
    finally:
        session.close()

def insert_data_qubit(branch, qubitData):
    try:
        engine = dolt_checkout(branch)
        Session = sessionmaker(bind=engine)
        session = Session()
        (qubit, gate, chip) = load_tables(engine)

        # Sanitize the input data
        for key, value in qubitData.items():
            if value == '' or value == 'null' or str(value).lower() == 'null':
                qubitData[key] = None

        with session.begin():
            # Check if the row exists
            existing_qubit = session.query(qubit).filter(
                qubit.c.name == qubitData["name"],  # Assuming the qubit name is stored in 'name' column
                qubit.c.chip_id == qubitData["chip_id"]
            ).first()

            if existing_qubit:
                # Update the existing row
                stmt = update(qubit).where(
                    qubit.c.name == qubitData["name"],
                    qubit.c.chip_id == qubitData["chip_id"]
                ).values(qubitData)
            else:
                # Insert a new row
                stmt = insert(qubit).values(qubitData)

            session.execute(stmt)
        session.commit()
        return jsonify({'message': 'Qubit data inserted or updated successfully'})
    except Exception as e:
        session.rollback()
        error_message = {
            'status': 'error',
            'message': 'Insert/update qubit error',
            'error_details': str(e)
        }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400
    finally:
        session.close()


def get_chip_data(branch, chip_id=None):
    try:
        engine = dolt_checkout(branch)  # Assuming dolt_checkout is a function you've previously defined
        Session = sessionmaker(bind=engine)
        session = Session()
        (qubit, gate, chip) = load_tables(engine)
        chip_column_names = chip.c.keys()
        stmt = text("SELECT * FROM chip")
        chip_data = session.execute(stmt)
        chip_data_list = []
        for row in chip_data:
            if row: 
                row_dict = dict(zip(chip_column_names, row))
                chip_data_list.append(row_dict)
        return jsonify(chip_data_list)
    except Exception as e:
        error_message = {
                    'status': 'error',
                    'message': 'get chip error',
                    'error_details': str(e)
                }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400
    finally:
        if session:
            session.close()

def get_all_data(branch,chip_id):
    try:
        engine = dolt_checkout(branch)  # Assuming this function checks out the correct branch and returns an engine
        Session = sessionmaker(bind=engine)
        session = Session()
        commit_logs = print_commit_log(branch)
        data = []
        for i in range(len(commit_logs) - 1):  # Exclude the last commit
            commit_log = commit_logs[i]
            commit_hash = commit_log['commit_hash']

            stmt1 = text("SELECT dolt_diff_gate.*, chip.name AS chip_name from dolt_diff_gate INNER JOIN chip ON dolt_diff_gate.to_chip_id = chip.id WHERE to_commit = :commit_hash and to_chip_id=:chip_id")
            stmt2 = text("SELECT dolt_diff_qubit.*, chip.name AS chip_name from dolt_diff_qubit INNER JOIN chip ON dolt_diff_qubit.to_chip_id = chip.id WHERE to_commit = :commit_hash and to_chip_id=:chip_id")
            gate_data = session.execute(stmt1, {'commit_hash': commit_hash,'chip_id': chip_id})
            qubit_data = session.execute(stmt2, {'commit_hash': commit_hash,'chip_id': chip_id})

            gate_data_list = []
            for row in gate_data:
                if row: 
                    row_dict = row._asdict()
                    gate_data_list.append(row_dict)
                else: 
                    print("empty row")

            qubit_data_list = []
            for row in qubit_data:
                if row: 
                    row_dict = row._asdict()
                    qubit_data_list.append(row_dict)
                else: 
                    print("empty row")

            commit_data = {
                'commit_hash': commit_hash,
                'author':commit_log['author'],
                'message':commit_log['message'],
                'date':commit_log['date']
            }

            if gate_data_list:
                commit_data['gate_data'] = gate_data_list
            
            # Only add qubit_data_list to commit_data if it is not empty
            if qubit_data_list:
                commit_data['qubit_data'] = qubit_data_list

            # Only append commit_data to data if either gate_data_list or qubit_data_list is not empty
            if gate_data_list or qubit_data_list:
                data.append(commit_data)
        return jsonify(data)

    except Exception as e:
        if session is not None:
             session.close()
        error_message = {
            'status': 'error',
            'message': 'get all data',
            'error_details': str(e)
        }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400
 
def get_all_data_by_property(branch,chip_id):
    try: 
        engine = dolt_checkout(branch)
        Session = sessionmaker(bind=engine)
        session = Session()
        commit_logs = print_commit_log(branch)
        data = []
        qubit_data_dict = {}
        gate_data_dict = {}

        for i in range(len(commit_logs) - 1):  # Exclude the last commit
            commit_log = commit_logs[i]
            commit_hash = commit_log['commit_hash']
            stmt1 = text("SELECT dolt_diff_gate.*, chip.name AS chip_name from dolt_diff_gate INNER JOIN chip ON dolt_diff_gate.to_chip_id = chip.id WHERE to_commit = :commit_hash and to_chip_id=:chip_id")
            stmt2 = text("SELECT dolt_diff_qubit.*, chip.name AS chip_name from dolt_diff_qubit INNER JOIN chip ON dolt_diff_qubit.to_chip_id = chip.id WHERE to_commit = :commit_hash and to_chip_id=:chip_id")
            with session.begin(): 
                gate_data = session.execute(stmt1, {'commit_hash': commit_hash, 'chip_id': chip_id})
                qubit_data = session.execute(stmt2, {'commit_hash': commit_hash, 'chip_id': chip_id})
                for row in gate_data:
                    row_dict = row._asdict()
                    current_gate_name = row_dict['to_gate_name']

                    if current_gate_name not in gate_data_dict:
                        gate_data_dict[current_gate_name] = []

                    gate_data_dict[current_gate_name].append({
                        "commit_hash": commit_hash,
                        "dest": row_dict['to_dest'],
                        "phase": row_dict['to_phase'],
                        "t0": row_dict['to_t0'],
                        "freq": row_dict['to_freq'],
                        "env": row_dict['to_env'],
                        "amp": row_dict['to_amp'],
                        "twidth": row_dict['to_twidth'],
                        "chip_id": row_dict['to_chip_id'],
                        "chip_name": row_dict['chip_name']
                    })
                                           
                for row in qubit_data:
                    row_dict = row._asdict()
                    current_qubit_name = row_dict['to_name']
                    if current_qubit_name not in qubit_data_dict:
                        qubit_data_dict[current_qubit_name] = []

                    qubit_data_dict[current_qubit_name].append({
                        "commit_hash": commit_hash,
                        "freq": row_dict['to_freq'],
                        "freq_ef": row_dict['to_freq_ef'],
                        "freq_predicted": row_dict['to_freq_predicted'],
                        "id": row_dict['to_id'],
                        "readfreq": row_dict['to_readfreq'],
                        "chip_id": row_dict['to_chip_id'],
                        "chip_name": row_dict['chip_name']                        
                    })

        # Create the desired response structure
        for qubit_name, qubit_data_list in qubit_data_dict.items():
            data.append({
                "name": qubit_name,
                "qubit_data": qubit_data_list
            })
        
        for gate_name, gate_data_list in gate_data_dict.items():
            data.append({
                "name": gate_name,
                "gate_data": gate_data_list
            })
        
        return jsonify(data)
    except Exception as e:
        if session is not None:
                session.close()
        error_message = {
            'status': 'error',
            'message': 'get all data',
            'error_details': str(e)
        }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400


def print_commit_log(branch):
    try:
        metadata_obj = MetaData()
        engine = dolt_checkout(branch)
        session = Session(bind=engine)
        dolt_log = Table("dolt_log", metadata_obj, autoload_with=engine)
        stmt = select(dolt_log.c.commit_hash,
                    dolt_log.c.committer,
                    dolt_log.c.message,
                    dolt_log.c.date
                    ).order_by(dolt_log.c.date.desc())
        rows=[]
        with session.begin():
            results = session.execute(stmt)
            for row in results:
                commit_hash = row[0]
                author      = row[1]
                message     = row[2]
                date     = row[3]
                rows.append({
                    'commit_hash':commit_hash,
                    'author':author,
                    'message':message,
                    'date':date
                })
            return rows
    except Exception as e:
        if session is not None:
                session.close()
        error_message = {
            'status': 'error',
            'message': 'print commit',
            'error_details': str(e)
        }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400

def get_commit_specific_data(hash,branch_name):
    try:
        #stmt1 = text(f"SELECT `calibration/{branch_name}`.dolt_diff_gate.*, chip.name AS chip_name FROM `calibration/{branch_name}`.dolt_diff_gate INNER JOIN `calibration/{branch_name}`.chip ON `calibration/{branch_name}`.dolt_diff_gate.to_chip_id = chip.id where to_commit=:commit_hash")
       # stmt2 = text(f"SELECT `calibration/{branch_name}`.dolt_diff_qubit.*, chip.name AS chip_name FROM `calibration/{branch_name}`.dolt_diff_qubit INNER JOIN `calibration/{branch_name}`.chip ON `calibration/{branch_name}`.dolt_diff_qubit.to_chip_id = chip.id where to_commit=:commit_hash")
        stmt1 = text(f"SELECT `calibration/{branch_name}`.gate.*, chip.name AS chip_name FROM `calibration/{branch_name}`.gate as of :commit_hash INNER JOIN `calibration/{branch_name}`.chip ON `calibration/{branch_name}`.gate.chip_id = chip.id ")
        stmt2 = text(f"SELECT `calibration/{branch_name}`.qubit.*, chip.name AS chip_name FROM `calibration/{branch_name}`.qubit as of :commit_hash INNER JOIN `calibration/{branch_name}`.chip ON `calibration/{branch_name}`.qubit.chip_id = chip.id")
        session = Session()
        with session.begin():
            gate_data = session.execute(stmt1, {'commit_hash': hash})
            qubit_data = session.execute(stmt2, {'commit_hash': hash})
   
        gate_data_list = [row._asdict() for row in gate_data]  # List to store gate data
        qubit_data_list = [row._asdict() for row in qubit_data]  # List to store qubit data

        return jsonify({
            'gate_data': gate_data_list,
            'qubit_data': qubit_data_list
        })
    except Exception as e:
        if session is not None:
                session.close()
        error_message = {
            'status': 'error',
            'message': 'get specific commit',
            'error_details': str(e)
        }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400
    
def get_commit_diff(hash, branch_name):
    try:
        stmt1 = text(f"SELECT * FROM `calibration/{branch_name}`.dolt_diff_gate where  to_commit = '" + hash + "'")
        stmt2 = text(f"SELECT * FROM `calibration/{branch_name}`.dolt_diff_qubit where  to_commit= '" + hash + "'")
        session = Session()
        with session.begin():
            gate_data = session.execute(stmt1)
            qubit_data = session.execute(stmt2)
            gate_column_names = gate_data.keys()
            qubit_column_names = qubit_data.keys()

        gate_data_dict = {}  # Dictionary to store latest gate data by gate_name
        for row in gate_data:
            row_dict = dict(zip(gate_column_names, row))
            current_gate_name = row_dict['to_gate_name']
            gate_data_dict[current_gate_name] = row_dict

        qubit_data_dict = {}  # Dictionary to store latest qubit data by name
        for row in qubit_data:
            row_dict = dict(zip(qubit_column_names, row))
            current_qubit_name = row_dict['to_name']
            qubit_data_dict[current_qubit_name] = row_dict
        session.commit()
        return jsonify({
            'gate_data': list(gate_data_dict.values()),
            'qubit_data': list(qubit_data_dict.values())
        })
    except Exception as e:
        if session is not None:
                session.close()
        error_message = {
            'status': 'error',
            'message': 'get commit hash',
            'error_details': str(e)
        }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400

def dolt_commit(engine, author, message):
    Session = sessionmaker(bind=engine)
    session = Session()
    with session.begin():
        session.execute(
            text("CALL DOLT_ADD('-A')")
        )
        result = session.execute(
            text("CALL DOLT_COMMIT('--allow-empty','--author', '"
                    + author
                    + "', '-am', '"
                    + message
                    + "')")
        )
        commit = None
        for row in result:
            commit = row[0]
        if ( commit ): 
            print("Created commit: " + commit )

def check_data(branch):
    try:
        Session = sessionmaker(bind=engine)
        session = Session()
        dolt_checkout(branch)
        stmt1 = text("select id from qubit limit 1")
        stmt2 = text("select id from gate limit 1")
        with session.begin():
            qubit_result = session.execute(stmt1).fetchone()
            gate_result = session.execute(stmt2).fetchone()
        session.commit()
         # Extract the first column from each result or return None if no rows were returned
        qubit_id = qubit_result[0] if qubit_result else None
        gate_id = gate_result[0] if gate_result else None
        return str(qubit_id), str(gate_id)
    except Exception as e:
        session.rollback()
        error_message = {
            'status': 'error',
            'message': 'get all data',
            'error_details': str(e)
        }
        logging.error("An error occurred: %s", str(e))
        return jsonify(error_message), 400
    finally:
        session.close()
# def get_all_data_by_property(branch):
#     try: 
#         engine = dolt_checkout(branch)
#         Session = sessionmaker(bind=engine)
#         session = Session()
#         (qubit, gate, chip) = load_tables(engine)
#         commit_logs = print_commit_log(branch)
#         gate_column_names = gate.c.keys()
#         qubit_column_names = qubit.c.keys()
#         data = []
#         qubit_data_dict = {}

#         for i in range(len(commit_logs) - 1):  # Exclude the last commit
#             commit_log = commit_logs[i]
#             commit_hash = commit_log['commit_hash']
#             stmt1 = text("SELECT * FROM gate AS OF '" + commit_hash + "'")
#             stmt2 = text("SELECT * FROM qubit AS OF '" + commit_hash + "'")
#             with session.begin():
#                 gate_data = session.execute(stmt1)
#                 qubit_data = session.execute(stmt2)
#                 gate_column_names = gate_data.keys()
#                 qubit_column_names = qubit_data.keys()

#                 gate_data_list = []
#                 gate_name_count = {}
#                 for row in gate_data:
#                     row_dict = dict(zip(gate_column_names, row))
#                     current_gate_name = row_dict['gate_name']
#                     if current_gate_name == 'M0mark':
#                         gate_name_count[current_gate_name] = gate_name_count.get(current_gate_name, 0) + 1
#                         if gate_name_count[current_gate_name] > 1:
#                             break
#                     gate_data_list.append(row_dict)

#                 qubit_name_count = {}
#                 for row in qubit_data:
#                     row_dict = dict(zip(qubit_column_names, row))
#                     current_qubit_name = row_dict['name']
#                     if current_qubit_name not in qubit_data_dict:
#                         qubit_data_dict[current_qubit_name] = []

#                     qubit_data_dict[current_qubit_name].append({
#                         "commit_hash": commit_hash,
#                         "freq": row_dict['freq'],
#                         "freq_ef": row_dict['freq_ef'],
#                         "freq_predicted": row_dict['freq_predicted'],
#                         "id": row_dict['id'],
#                         "readfreq": row_dict['readfreq']
#                     })

#         # Create the desired response structure
#         for qubit_name, qubit_data_list in qubit_data_dict.items():
#             data.append({
#                 "name": qubit_name,
#                 "qubit_data": qubit_data_list
#             })

#         print(data)
#         session.commit()
#         return jsonify(data)
#     except Exception as e:
#         session.rollback()
#         raise e
#     finally:
#         session.close()

# def get_all_data_by_property(branch):
#     try: 
#         engine = dolt_checkout(branch)
#         Session = sessionmaker(bind=engine)
#         session = Session()
#         (qubit, gate, chip) = load_tables(engine)
#         commit_logs = print_commit_log(branch)
#         gate_column_names = gate.c.keys()
#         qubit_column_names = qubit.c.keys()
#         data = []
#         qubit_data_dict = {}

#         for i in range(len(commit_logs) - 1):  # Exclude the last commit
#             commit_log = commit_logs[i]
#             commit_hash = commit_log['commit_hash']
#             stmt1 = text("SELECT * FROM gate AS OF '" + commit_hash + "'")
#             stmt2 = text("SELECT * FROM qubit AS OF '" + commit_hash + "'")
#             with session.begin():
#                 gate_data = session.execute(stmt1)
#                 qubit_data = session.execute(stmt2)
#                 gate_column_names = gate_data.keys()
#                 qubit_column_names = qubit_data.keys()

#                 gate_data_list = []
#                 gate_name_count = {}
#                 for row in gate_data:
#                     row_dict = dict(zip(gate_column_names, row))
#                     current_gate_name = row_dict['gate_name']
#                     if current_gate_name == 'M0mark':
#                         gate_name_count[current_gate_name] = gate_name_count.get(current_gate_name, 0) + 1
#                         if gate_name_count[current_gate_name] > 1:
#                             break
#                     gate_data_list.append(row_dict)

#                 qubit_name_count = {}
#                 for row in qubit_data:
#                     row_dict = dict(zip(qubit_column_names, row))
#                     current_qubit_name = row_dict['name']
#                     if current_qubit_name not in qubit_data_dict:
#                         qubit_data_dict[current_qubit_name] = []

#                     qubit_data_dict[current_qubit_name].append({
#                         "commit_hash": commit_hash,
#                         "freq": row_dict['freq'],
#                         "freq_ef": row_dict['freq_ef'],
#                         "freq_predicted": row_dict['freq_predicted'],
#                         "id": row_dict['id'],
#                         "readfreq": row_dict['readfreq']
#                     })

#         # Create the desired response structure
#         for qubit_name, qubit_data_list in qubit_data_dict.items():
#             data.append({
#                 "name": qubit_name,
#                 "qubit_data": qubit_data_list
#             })

#         print(data)
#         session.commit()
#         return jsonify(data)
#     except Exception as e:
#         session.rollback()
#         raise e
#     finally:
#         session.close()