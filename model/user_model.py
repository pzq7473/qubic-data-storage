# model.py

from flask import jsonify
from config.db import mongo
from bson import ObjectId

class User:
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def save(self):
        users = mongo.db.users
        user = {"username": self.username, "password": self.password}
        users.insert_one(user)

    @staticmethod
    def get_all():
        users = mongo.db.users
        result = users.find()
        json_result = []
        for user in result:
            user['_id'] = str(user['_id'])
            json_result.append(user)
        return json_result

    @staticmethod
    def get_by_username(username):
        users = mongo.db.users
        result = users.find_one({"username": username})
        if result:
            result['_id'] = str(result['_id'])  # Convert ObjectId to string
        return result
