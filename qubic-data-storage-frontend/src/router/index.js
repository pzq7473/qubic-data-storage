// Composables
import { createRouter, createWebHistory } from 'vue-router'


const routeProps = (queryProps) => (route) => queryProps.reduce((props, prop) => {
  props[prop] = route.query[prop];
  return props;
}, {});

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        name: 'Home',
        component: () => import('@/views/Home.vue'),
      },
      {
        path: '/allExperimentData',
        name: 'allExperimentData',
        component: () => import('@/views/visualization/byExperiment/allExperiments.vue'),
      },

      // Visualization
      {
        path: '/visualization',
        name: 'Visualization',
        component: () => import('@/views/visualization/index.vue'),
      },
      {
        path: '/visualization/byExperimentData',
        name: 'visualizationByExperimentData',
        props: routeProps(['chip_name']),
        component: () => import('@/views/visualization/byExperiment/index.vue'),
      },
      {
        path: '/visualization/byCalibrationData',
        name: 'visualizationByCalibrationData',
        props: routeProps(['branch_name', 'chip_name']),
        component: () => import('@/views/visualization/byCalibration/index_chips.vue'),
      },
      {
        path: '/visualization/byCalibrationData/eachBranch',
        name: 'visualizationByCalibrationDataEachBranch',
        props: routeProps(['branch_name']),
        component: () => import('@/views/visualization/byCalibration/byCalibration.vue'),
        children: []
      },

      {
        path: '/visualization/calibration/bycommit/qubit',
        name: 'visualizationByCalibrationDatByCommitqubit',
        props: routeProps(['branch_name', 'chip_id']),
        component: () => import('@/views/visualization/byCalibration/bycommit/qubit.vue'),
        children: []
      },
      {
        path: '/visualization/calibration/bycommit/gate_phase',
        name: 'visualizationByCalibrationDatByCommitgate_phase',
        props: routeProps(['branch_name', 'chip_id']),
        component: () => import('@/views/visualization/byCalibration/bycommit/gate_phase.vue'),
        children: []
      },
      {
        path: '/visualization/calibration/bycommit/gate_freq',
        name: 'visualizationByCalibrationDatByCommitgate_freq',
        props: routeProps(['branch_name', 'chip_id']),
        component: () => import('@/views/visualization/byCalibration/bycommit/gate_freq.vue'),
        children: []
      },
      {
        path: '/visualization/calibration/bycommit/gate_amp',
        name: 'visualizationByCalibrationDatByCommitgate_amp',
        props: routeProps(['branch_name', 'chip_id']),
        component: () => import('@/views/visualization/byCalibration/bycommit/gate_amp.vue'),
        children: []
      },
      {
        path: '/visualization/calibration/bycommit/gate_twidth',
        name: 'visualizationByCalibrationDatByCommitgate_twidth',
        props: routeProps(['branch_name', 'chip_id']),
        component: () => import('@/views/visualization/byCalibration/bycommit/gate_twidth.vue'),
        children: []
      },
      {
        path: '/visualization/calibration/byproperty/qubit',
        name: 'visualizationByCalibrationDataEachBranchByPropertyqubit',
        props: routeProps(['branch_name', 'chip_id']),
        component: () => import('@/views/visualization/byCalibration/byproperty/qubit.vue'),
        children: []
      },
      {
        path: '/visualization/calibration/byproperty/gate_phase',
        name: 'visualizationByCalibrationDataEachBranchByPropertygate_phase',
        props: routeProps(['branch_name', 'chip_id']),
        component: () => import('@/views/visualization/byCalibration/byproperty/gate_phase.vue'),
        children: []
      },
      {
        path: '/visualization/calibration/byproperty/gate_freq',
        name: 'visualizationByCalibrationDataEachBranchByPropertygate_freq',
        props: routeProps(['branch_name', 'chip_id']),
        component: () => import('@/views/visualization/byCalibration/byproperty/gate_freq.vue'),
        children: []
      },
      {
        path: '/visualization/calibration/byproperty/gate_amp',
        name: 'visualizationByCalibrationDataEachBranchByPropertygate_amp',
        props: routeProps(['branch_name', 'chip_id']),
        component: () => import('@/views/visualization/byCalibration/byproperty/gate_amp.vue'),
        children: []
      },
      {
        path: '/visualization/calibration/byproperty/gate_twidth',
        name: 'visualizationByCalibrationDataEachBranchByPropertygate_twidth',
        props: routeProps(['branch_name', 'chip_id']),
        component: () => import('@/views/visualization/byCalibration/byproperty/gate_twidth.vue'),
        children: []
      },
      {
        path: '/visualization/calibration/byproperty',
        name: 'visualizationByCalibrationDataEachBranchByProperty',
        props: routeProps(['branch_name']),
        component: () => import('@/views/visualization/byCalibration/byCalibrationProperty.vue'),
        children: []
      },
      {
        path: '/visualization/byQubits',
        name: 'visualizationByQubits',
        props: routeProps(['chip_name']),
        component: () => import('@/views/visualization/byQubits/index.vue'),
      },


      // Calibration
      {
        path: '/calibration',
        name: 'Calibration',
        component: () => import('@/views/calibration/index.vue'),
        children: []
      },
      {
        path: '/calibration/newExperiment',
        name: 'NewExperiment',
        props: (route) => ({
          requestData: route.query
        }),
        component: () => import('@/views/calibration/calibrationForm.vue'),
        children: []
      },
      {
        path: '/calibration/updateExperiment',
        name: 'UpdateExperiment',
        props: routeProps(['branch_name']),
        component: () => import('@/views/calibration/updateCalibrationForm.vue'),
        children: []
      },
      {
        path: '/calibration/viewExperimentData',
        name: 'ViewExperimentData',
        props: routeProps(['commit_hash','branch_name']),
        component: () => import('@/views/calibration/viewCalibrationData.vue'),
        children: []
      },
      {
        path: '/dataset',
        name: 'Dataset',
        component: () => import('@/views/Home.vue'),
      },
      {
        path: '/fidelity',
        name: 'fidelity',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byExperiment/InfidelityGraph.vue'),
      },
      {
        path: '/prep0read1',
        name: 'prep0read1',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byExperiment/perp0read1Graph.vue'),
      },
      {
        path: '/prep1read0',
        name: 'prep1read0',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byExperiment/perp1read0Graph.vue'),
      },
      {
        path: '/t1',
        name: 't1',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byExperiment/t1Graph.vue'),
      },
      {
        path: '/t2spinecho',
        name: 't2spinecho',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byExperiment/t2spinechoGraph.vue'),
      },
      {
        path: '/t2ramsey',
        name: 't2ramsey',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byExperiment/t2ramseyGraph.vue'),
      },
      {
        path: '/separation',
        name: 'separation',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byExperiment/seperationGraph.vue'),
      },
      {
        path: '/q0',
        name: 'q0',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byQubits/Q0Graph.vue'),
      },
      {
        path: '/q1',
        name: 'q1',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byQubits/Q1Graph.vue'),
      },
      {
        path: '/q2',
        name: 'q2',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byQubits/Q2Graph.vue'),
      },
      {
        path: '/q3',
        name: 'q3',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byQubits/Q3Graph.vue'),
      },
      {
        path: '/q4',
        name: 'q4',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byQubits/Q4Graph.vue'),
      },
      {
        path: '/q5',
        name: 'q5',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byQubits/Q5Graph.vue'),
      },
      {
        path: '/q6',
        name: 'q6',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byQubits/Q6Graph.vue'),
      },
      {
        path: '/q7',
        name: 'q7',
        props: routeProps(['chip_name']),
        component: () => import(/* webpackChunkName: "" */ '@/views/visualization/byQubits/Q7Graph.vue'),
      },
      {
        path: '/test',
        name: 'test',
        component: () => import(/* webpackChunkName: "" */ '@/components/testGraph.vue'),
      },
    ],
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
