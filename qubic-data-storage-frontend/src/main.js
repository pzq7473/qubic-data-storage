/**
 * main.js
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Components
import App from './App.vue'

// Composables
import { createApp } from 'vue'

// Plugins
import { registerPlugins } from '@/plugins'


// import { VDataTable } from 'vuetify/lib'

const app = createApp(App)

// app.component('v-data-table', VDataTable)

registerPlugins(app)

app.mount('#app')

